import { Component, AfterViewInit } from '@angular/core';
import {TemplateLibrariesLoaderService} from "./template-libraries-loader.service";


@Component({
    selector : 'child',
    template : `<div id="child">Child hello!</div>`
})
export class ChildComponent implements AfterViewInit{
    ngAfterViewInit():any {
        this.templateLibLoader.loadLibraries([
            '/libs/child.js'
        ]);
    }

    constructor(private templateLibLoader : TemplateLibrariesLoaderService){

    }

}