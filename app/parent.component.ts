import { Component, AfterViewInit } from '@angular/core';
import {ChildComponent} from "./child.component";
import {TemplateLibrariesLoaderService} from "./template-libraries-loader.service";


@Component({
    selector : 'parent',
    directives : [ ChildComponent],
    providers : [ TemplateLibrariesLoaderService ],
    template : `<div id="parent">Parent hello</div> <child></child>`
})
export class ParentComponent implements AfterViewInit{
    ngAfterViewInit():any {
        this.templateLibLoader.loadLibraries([
            '/libs/parent.js',
            '/libs/parent2.js'
        ]);
    }

    constructor(private templateLibLoader : TemplateLibrariesLoaderService){

    }
}