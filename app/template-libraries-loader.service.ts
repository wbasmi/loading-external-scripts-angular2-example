import { Injectable, ElementRef } from '@angular/core';


@Injectable()
export class TemplateLibrariesLoaderService{

    loads = null;


    loadLibraries(libs : string[]):any{

        if( libs.length == 0) return;
        let start = 0;

        if(!this.loads)
        {
            this.loads = this.Import(libs[0]);
            start=1;
        }

        for (let i =start ; i< libs.length; ++i)
        {
            this.loads.then(() => this.Import(libs[i]));
        }
    }


    private Import(lib) : Promise<any>{
        console.log('importing ', lib);
        return new Promise( (resolve, reject) => {
            var xhrObj = new XMLHttpRequest();
            xhrObj.open('GET', lib, true);
            xhrObj.setRequestHeader('Content-Type', 'application/javascript');
            xhrObj.send();
            xhrObj.onreadystatechange = function(){
                if(xhrObj.readyState == 4){
                    try{
                        var execution = new Function(`return (function() {${xhrObj.responseText}})()`);
                        execution();
                    }catch(err) {
                        console.log('in ', lib);
                        console.log(err);
                    }
                    resolve();
                    return;
                }
            };
        }).catch(err => console.error('something bad happened in the import promise'));
    }
}

